package fxlauncher;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.xml.bind.JAXB;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * client:
 * 1) main for client auto-update and client program start
 * 2) need parameter --app=http://.../../app.xml
 * 3) parse attribute startBatches in app.xml, attribute string divide by ';', 
 */
public class Launcher extends Application {
    private static final Logger log = Logger.getLogger("Launcher");

    private FXManifest manifest;
    private Application app;
    private StackPane root;
    private ProgressBar progressBar;
    private Stage stage;
    private String phase;

    public void start(Stage primaryStage) throws Exception {

        root = new StackPane(new ProgressIndicator());
        root.setPrefSize(200, 80);
        root.setPadding(new Insets(10));

        stage = new Stage(StageStyle.UNDECORATED);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        

        new Thread(() -> {
	        Thread.currentThread().setName("FXLauncher-Thread");
            try {
                updateManifest();
                createUpdateWrapper();
	            Path cacheDir = manifest.resolveCacheDir(getParameters().getNamed());
	            log.info(String.format("Using cache dir %s", cacheDir.toAbsolutePath().toString()));
	            syncFiles(cacheDir);
            } catch (Exception ex) {
                log.log(Level.WARNING, String.format("Error during %s phase", phase), ex);
            }
            

            if (manifest.startBatches != null) {
                try {
                	for (String bat : manifest.startBatches.split(";")){
                        System.out.println("Start " + bat + " ...");
                        this.runChild(bat);
                	}
//                    createApplication();
//                    launchAppFromManifest();
                } catch (Exception ex) {
                    reportError(String.format("Error during %s phase", phase), ex);
                }
            }

            System.exit(0);
        }).start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void createUpdateWrapper() {
        phase = "Update Wrapper Creation";

        Platform.runLater(() -> {
            progressBar = new ProgressBar();
            progressBar.setStyle(manifest.progressBarStyle);

            Label label = new Label(manifest.updateText);
            label.setStyle(manifest.updateLabelStyle);

            VBox wrapper = new VBox(label, progressBar);
            wrapper.setStyle(manifest.wrapperStyle);

            root.getChildren().clear();
            root.getChildren().add(wrapper);
        });
    }


    private void updateManifest() throws Exception {
        phase = "Update Manifest";
        syncManifest();
    }

    private void syncFiles(Path cacheDir) throws Exception {
        phase = "File Synchronization";

        List<LibraryFile> needsUpdate = manifest.files.stream()
                .filter(LibraryFile::loadForCurrentPlatform)
                .filter(it -> it.needsUpdate(cacheDir))
                .collect(Collectors.toList());

        Long totalBytes = needsUpdate.stream().mapToLong(f -> f.size).sum();
        Long totalWritten = 0L;

        System.out.println("manifest.uri=" + manifest.uri);
        for (LibraryFile lib : needsUpdate) {
            Path target = cacheDir.resolve(lib.file).toAbsolutePath();
            Files.createDirectories(target.getParent());

            System.out.println("lib.file   : " + lib.file);
            System.out.println("target path: " + target.toString());

            try (InputStream input = manifest.uri.resolve(lib.file).toURL().openStream();
                 OutputStream output = Files.newOutputStream(target)) {

                byte[] buf = new byte[65536];

                int read;
                while ((read = input.read(buf)) > -1) {
                    output.write(buf, 0, read);
                    totalWritten += read;
                    Double progress = totalWritten.doubleValue() / totalBytes.doubleValue();
                    Platform.runLater(() -> progressBar.setProgress(progress));
                }
            }
        }
    }


    public void stop() throws Exception {
        if (app != null)
            app.stop();
    }

    private void reportError(String title, Throwable error) {
        log.log(Level.WARNING, title, error);

        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(title);
            alert.setHeaderText(title);
            alert.getDialogPane().setPrefWidth(600);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(out);
            error.printStackTrace(writer);
            writer.close();
            TextArea text = new TextArea(out.toString());
            alert.getDialogPane().setContent(text);

            alert.showAndWait();
            Platform.exit();
        });
    }

    private void syncManifest() throws Exception {
        Map<String, String> namedParams = getParameters().getNamed();

        if (namedParams.containsKey("app")) {
            String manifestURL = namedParams.get("app");
            log.info(String.format("Loading manifest from parameter supplied location %s", manifestURL));
            System.out.println("manifestURL=" + manifestURL);
            manifest = JAXB.unmarshal(URI.create(manifestURL), FXManifest.class);
            return;
        }

        URL embeddedManifest = Launcher.class.getResource("/app.xml");
        manifest = JAXB.unmarshal(embeddedManifest, FXManifest.class);

	    Path cacheDir = manifest.resolveCacheDir(namedParams);
	    Path manifestPath = manifest.getPath(cacheDir);

	    if (Files.exists(manifestPath))
            manifest = JAXB.unmarshal(manifestPath.toFile(), FXManifest.class);

        try {
            FXManifest remoteManifest = JAXB.unmarshal(manifest.getFXAppURI(), FXManifest.class);

            if (remoteManifest == null) {
                log.info(String.format("No remote manifest at %s", manifest.getFXAppURI()));
            } else if (!remoteManifest.equals(manifest)) {
                // Update to remote manifest if newer or we specifially accept downgrades
                if (remoteManifest.isNewerThan(manifest) || manifest.acceptDowngrade) {
                    manifest = remoteManifest;
                    JAXB.marshal(manifest, manifestPath.toFile());
                }
            }
        } catch (Exception ex) {
            log.log(Level.WARNING, "Unable to update manifest", ex);
        }
    }

    
    private void runChild(String runName) {
    	String childProcPath = findFilePath(runName);
    	
    	if (childProcPath != null)
			try {
				String dir = new File(childProcPath).getParent().toString();
				System.out.println("run path    = " + childProcPath);
				System.out.println("working dir = " + dir);
				System.out.println();
				Runtime.getRuntime().exec("cmd cd  " + dir);
				Runtime.getRuntime().exec("cmd /K start " + childProcPath, null, new File(dir));
			} catch (IOException e) {
				e.printStackTrace();
			}
    }
    private static String findFilePath(String filename) {
    	Path currentDir = new File(".").toPath();
    	String result = null;
        try {
        	SimpleFileVisitor<Path> pathFinder = new SimpleFileVisitor<Path>() {
				public String result;
				//override to return result
				public String toString() {
					return result;
				}
			    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			    	
			    	if (file.getFileName().toString().equalsIgnoreCase(filename)) {
			    		result = file.toAbsolutePath().toString();
			    		return FileVisitResult.TERMINATE;
			    	}
			        
			        return FileVisitResult.CONTINUE;
			    }
			};
			Files.walkFileTree(currentDir, pathFinder);
			result = pathFinder.toString();
			
			result = result.replace("\\.\\", "\\");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return result;
    }
}
