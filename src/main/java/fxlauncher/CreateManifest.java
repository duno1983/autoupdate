package fxlauncher;

import com.sun.javafx.application.ParametersImpl;

import javax.xml.bind.JAXB;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Generate app.xml:
 * 1) parameters & commandline usage:
		@set URI=http://localhost/app/
		@set MAIN_CLASS=com.pisolution.application.CrewMainFrame
		@set BUILD_DIR=D:\J2EE\apache-tomcat-8.0.23\webapps\app
		@set SRC_ROOT=GUI/copo/
		@set START_BATCH=01_RuleSrv.bat;02_mainframe.bat
		java -cp "fxlauncher.jar" fxlauncher.CreateManifest "%URI%" "%MAIN_CLASS%" "%BUILD_DIR%" "%SRC_ROOT%" "%START_BATCH%"
 * 2) include/exclude file type: includeExtensions/ notIncludeExtensions, '*' means all type
 * 3) boolean shouldIncludeInManifest (file): return if file should include in app.xml
 * 4) FXManifest class: store all parameters & auto-update target files
 */
public class CreateManifest {
    private static HashMap<String, String> includeExtensions = new HashMap<>();
    private static HashMap<String, String> notIncludeExtensions = new HashMap<>();

	static {
		Arrays.asList(
				//"jar", "war", "txt", "exe", "dll", "ini", "bat", "png", "class", "properties", "xml", "fxml", "css", "gif", "jpg"
				"*"
				).forEach(item -> includeExtensions.put(item, item));
		Arrays.asList(
				"log"
				).forEach(item -> notIncludeExtensions.put(item, item));
    }

    public static void main(String[] args) throws IOException {
        URI baseURI = URI.create(args[0]);
        String launchClass = args[1];
        Path appPath = Paths.get(args[2]);
        FXManifest manifest = create(baseURI, launchClass, appPath);

        System.out.println("**************************************************************");
        System.out.print("Support file type: ");
        for (String fileType : includeExtensions.keySet()) {
        	System.out.print(" ." + fileType);
        }
        System.out.println();
        System.out.println("**************************************************************");
        for (String arg : args) {
        	System.out.println("   Param: " + arg);
        }
        
        if (args.length > 3) {
            // Parse named parameters
            List<String> rawParams = new ArrayList<>();
            rawParams.addAll(Arrays.asList(args).subList(3, args.length));
            ParametersImpl params = new ParametersImpl(rawParams);
            Map<String, String> named = params.getNamed();

            if (named != null) {
                // Configure cacheDir
                if (named.containsKey("cache-dir"))
                    manifest.cacheDir = named.get("cache-dir");

                // Configure acceptDowngrade
                if (named.containsKey("accept-downgrade"))
                    manifest.acceptDowngrade = Boolean.valueOf(named.get("accept-downgrade"));

                // Add additional files with these extensions to manifest
                if (named.containsKey("include-extensions"))
                    Arrays.stream(named.get("include-extensions").split(","))
                            .filter(s -> s != null && !s.isEmpty())
                            .collect(Collectors.toList()).forEach(item -> {
                            	includeExtensions.put(item, item);
                            });
            }

            if (args.length >= 4) {
            	manifest.srcRoot = args[3];
            	System.out.println("srcRoot=" + manifest.srcRoot);
            }
            
            if (args.length >= 5) {
            	manifest.startBatches = args[4];
            	System.out.println("batch = " + manifest.startBatches);
            }

            // Append the rest as manifest parameters
            StringBuilder rest = new StringBuilder();
            for (String raw : params.getRaw()) {
                if (raw.startsWith("--cache-dir=")) continue;
                if (raw.startsWith("--accept-downgrade=")) continue;
                if (raw.startsWith("--include-extensions=")) continue;
                if (rest.length() > 0) rest.append(" ");
                rest.append(raw);
            }

            // Add the raw parameter string to the manifest
            if (rest.length() > 0)
                manifest.parameters = rest.toString();
        }

        JAXB.marshal(manifest, appPath.resolve("app.xml").toFile());
        
        System.out.println("done");
    }

    public static FXManifest create(URI baseURI, String launchClass, Path appPath) throws IOException {
        FXManifest manifest = new FXManifest();
        manifest.ts = System.currentTimeMillis();
        manifest.uri = baseURI;
        manifest.launchClass = launchClass;

        Map<String, String> notIncludedFileType = new HashMap<>();
        Files.walkFileTree(appPath, new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            	String fileType = getExtension(file.toString());
            	
                if (!Files.isDirectory(file) && shouldIncludeInManifest(file) && !file.getFileName().toString().startsWith("fxlauncher"))
                    manifest.files.add(new LibraryFile(appPath, file));
                else
                	notIncludedFileType.put(fileType, fileType);
                
                return FileVisitResult.CONTINUE;
            }
        });
        
        ///TEST
        System.out.println("**************************************************************");
        System.out.print("notIncludedFileType:");
        notIncludedFileType.forEach((k, v) -> {
        	System.out.print("  ." + k);
        });
        System.out.println("\n**************************************************************");

        return manifest;
    }

    private static String getExtension(String fileName) {
    	String extension = "";

    	int i = fileName.lastIndexOf('.');
    	if (i > 0) {
    	    extension = fileName.substring(i+1);
    	}
    	return extension;
    }
    
    private static boolean shouldIncludeInManifest(Path file) {
    	String ext = getExtension(file.toString());
    	if (notIncludeExtensions.containsKey(ext))
    		return false;
    	
    	if (includeExtensions.containsKey("*"))
    		return true;
    	
    	if (includeExtensions.containsKey(ext))
    		return true;
        return false;
    }

}
